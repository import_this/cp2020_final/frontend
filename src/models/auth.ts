import { axiosInstance } from "./axios";

export class Auth {
  static async logIn(pass: string) {
    const response = await axiosInstance.post(
      "/login",
      {
        password: pass
      },
      {
        baseURL: ""
      }
    );
    return response;
  }
}
