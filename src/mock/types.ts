import { Server, Model } from "miragejs";
import { Registry } from "miragejs/-types";
import Schema from "miragejs/orm/schema";
import { CvModel, CvFactory } from "./cv";
import { MessageModel, MessageFactory } from "./message";
import { UserFactory, UserModel } from "./user";
import { VacancyFactory, VacancyModel } from './vacancy';

export type AppRegistry = Registry<
  {
    cv: typeof CvModel;
    message: typeof MessageModel;
    user: typeof UserModel;
    vacancy: typeof VacancyModel;
  },
  {
    cv: typeof CvFactory;
    message: typeof MessageFactory;
    user: typeof UserFactory;
    vacancy: typeof VacancyFactory;
  }
>;

export type AppSchema = Schema<AppRegistry>;
export type AppServer = Server<AppRegistry>;
