import Vue from "vue";
import Vuex from "vuex";

import { snack } from "./modules/snack/";

import { RootState } from "./types";

Vue.use(Vuex);

export default new Vuex.Store<RootState>({
  state: {
    version: "0"
  },
  mutations: {},
  actions: {},
  modules: {
    snack
  }
});
