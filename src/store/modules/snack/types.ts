export interface SnackState {
  show: boolean;
  message: string;
  color: string;
  timeout: number;
}
